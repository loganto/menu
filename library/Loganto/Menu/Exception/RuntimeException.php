<?php

namespace Loganto\Menu\Exception;

class RuntimeException extends \RuntimeException implements \Loganto\Menu\Exception\ExceptionInterface
{}
