<?php

/*
	What singe menu entry might look like:
	array(
		'label' => 'My vrentals',
		'title' => 'My vrentals',
		'resource' => 'resource:menu:my_vrentals',
		'url'     => '/my_vrentals',
		'aAttrs'  => array(), 
		'liAttrs' => array(), 
		'ulAttrs' => array(),
		
		'aBefore' => 'some string - will be injected before a',
		'aAfter' => 'some string - will be injected after a',
		'liBefore' => 'some string - will be injected before li',
		'liAfter' => 'some string - will be injected after li',
		'ulBefore' => 'some string - will be injected before ul',
		'ulAfter' => 'some string - will be injected after ul',
	),
*/


/**
 * @namespace
 */
namespace Loganto;

class Menu
{
	/*
	 * Params having to do with ul naming
	 */
	private $mainUlAttributes = array();
	private $standardCssClass = 'menu';
	private $standardCssSubClass = 'sub-menu';
	private $addStandartUlClasses = true;
	private $numberStandartUlClasses = true;
	
	/*
	 * Params having to do with li naming
	 */
	private $currentMenuItemClass = 'current-menu-item';
	private $currentMenuAncestorClass = 'current-menu-ancestor';
	private $currentMenuParentClass = 'current-menu-parent';
	
	/*
	 * Figuring out what elements to render and what not
	 */
	private $acl = null;
	private $role = null;
	
	/*
	 * Menu array to generate menu from
	 */
	private $data = null;
	
	/*
	 * ZF2 translator - for labels and titles
	 */
	private $translator = null;
	
	/*
	 * Resource name of the active element
	 */
	private $active = null;
	
	/*
	 * Internal boolean that tells us if active class is already set
	 */
	private $alreadyPreParsed = false;
	
	/*
	 * Only ZF2 Acl is supported.
	 */
    public function setAcl(\Zend\Permissions\Acl\AclInterface $acl) 
    {
		$this->acl = $acl;
		return $this;
	}
	
	/*
	 * Current role
	 */
    public function setRole($role) 
    {
		$this->role = $role;
		return $this;
	}
	
	/*
	 * Only ZF2 Translator is supported.
	 */
    public function setTranslator(\Zend\I18n\Translator $translator) 
    {
		$this->translator = $translator;
		return $this;
	}
	
	/*
	 * Main container UL attributes
	 */
    public function setMainUlAttributes(array $attrs) 
    {
		$this->mainUlAttributes = $attrs;
		return $this;
	}
	
    public function setStandartCssClass($name)
    {
		$this->standardCssClass = $name;
		return $this;
	}
	
    public function setCurrentItemClass($name) 
    {
		$this->currentMenuItemClass = $name;
		return $this;
	}
	
    public function setAncestorItemClass($name) 
    {
		$this->currentMenuAncestorClass = $name;
		return $this;
	}
	
    public function setParentItemClass($name) 
    {
		$this->currentMenuParentClass = $name;
		return $this;
	}
	
    public function setStandardCssSubClass($sub) 
    {
		$this->standardCssSubClass = $sub;
		return $this;
	}
	
	/*
	 * Should ul elements have standard css classes added
	 */
    public function setAddStandartUlClasses($tf)
    {
		$this->addStandartUlClasses = (bool)$tf;
		return $this;
	}
	
	/*
	 * Should ul elements have standard css classess numbered
	 */
    public function setNumberStandartUlClasses($tf)
    {
		$this->numberStandartUlClasses = (bool)$tf;
		return $this;
	}

    public function setData(array $cfg) 
    {
		$this->data = $cfg;
		return $this;
	}

    public function setActive($act) 
    {
		$this->active = $act;
		return $this;
	}
	
    public function generateMenu() 
    {
		/*
		 * If no initial data - return empty string
		 */
		if (empty($this->data)) {
			return '';
		}
		
		/*
		 * Adding active class to active element and active holder classess to ul ancestors
		 */
		if (!$this->alreadyPreParsed) {
			/*
			 * So not to parse it again
			 */
			$this->alreadyPreParsed = true;
			
			/*
			 * Finding active item hierarchy
			 */
			if ($this->active !== null) {
				$hk = $this->getItemHierarchy($this->active, $this->data);
				if ($hk !== false) {
					/*
					 * Highlight active item
					 */
					$cfgArray = null;
					while (count($hk) > 0) {
						$currentIndex = array_shift($hk);
						if ($cfgArray === null) {
							$cfgArray = &$this->data[$currentIndex];
						} else {
							$cfgArray = &$cfgArray[$currentIndex];
						}
						
						/*
						 * We obviously can than skip this iteration if element is mere holder
						 */
						if ($currentIndex === 'sub') {
							continue;
						}
						
						/*
						 * Create liAttrs array if not exist
						 */
						if(!array_key_exists('liAttrs', $cfgArray) || !is_array($cfgArray['liAttrs'])) {
							$cfgArray['liAttrs'] = array();
						}
						
						if (count($hk) === 0) {
							$this->addClass($cfgArray['liAttrs'], $this->currentMenuItemClass);
						} else {
							$this->addClass($cfgArray['liAttrs'], (count($hk) > 2) ? $this->currentMenuAncestorClass : $this->currentMenuParentClass);
						}
					}
				}
			}
		}
		
		/*
		 * Build menu recursively (strating from top level 1 and root config )
		 */
		return $this->recursiveMenu(1, $this->data, $this->mainUlAttributes);
	}
	

	/*
	 * Scans a multidimensional array and returns an array of keys that lead to our item.
	 * ex: [3][sub][2][sub][7]
	 */
	private function getItemHierarchy($valueToSearchFor, array $arrayToSearchThrough, array $currentKeysChain=array())
	{
		if (in_array($valueToSearchFor, array_values($arrayToSearchThrough))) {
			/*
			 * Adding found value key to $currentKeysChain array. Scan ends here.
			 */
			return $currentKeysChain;
		}
		
		/*
		 * If not immediately found - we go for recursive scan of internal arrays.
		 */
		foreach ($arrayToSearchThrough as $key => $menuItem) {
			if (is_array($menuItem)) {
				$k = $currentKeysChain;
				$k[] = $key;
				if ($find = $this->getItemHierarchy($valueToSearchFor, $menuItem, $k)) {
					return $find;
				}
			}
		}

		/*
		 * Active item not found
		 */
		return false;
	}

	/*
	 * Adding a css class to a li element
	 */
    private function addClass(&$itemProps, $className) 
    {
		if (!array_key_exists('class', $itemProps) || !is_array($itemProps)) {
			$itemProps['class'] = '';
		}
		
		$itemProps['class'] = empty($itemProps['class']) ? $className : ($itemProps['class'] . " $className");
	}

	/*
	 * Recursively building a menu
	 */
    private function recursiveMenu($cnt, $submittedCfg, $ula = array())
    {
		
		/*
		 * Check if ACL enabled - and remove all forbidden resources
		 */
		if (($this->acl !== null) && ($this->role !== null)) {
			foreach ($submittedCfg as $index => $itemProps) {
				if (array_key_exists('resource', $itemProps) && !$this->acl->isAllowed($this->role, $itemProps['resource'])) {
					unset($submittedCfg[$index]);
				}
			}
		}
		
		/*
		 * Adding standard classes and numbering (if required)
		 */
		if ($this->addStandartUlClasses) {
			$this->addClass($ula, (($cnt === 1) ? $this->standardCssClass : $this->standardCssSubClass) . ($this->numberStandartUlClasses ? $cnt : '' ));
		}
		
		/*
		 * Generating ul attributes line
		 */
		$ulaText = $this->generateAttributesLine($ula);
		
	    /*
		 * menu (root ul has class ex: "menu1", each submenu gets number incremented)
		 */
		$menu = '<ul' . $ulaText . '>';
		
		$submittedCfg = array_values($submittedCfg);
		
		/*
		 * Loop through the menu items
		 */
		$totalLength = count($submittedCfg);
		foreach ($submittedCfg as $index => $itemProps) {
			
			/*
			 * Create liAttrs array if not exist
			 */
			if(!array_key_exists('liAttrs', $itemProps) || !is_array($itemProps['liAttrs'])) {
				$itemProps['liAttrs'] = array();
			}
			
			/*
			 * If first li - we add "first" class, last li - we add last class
			 */
			if ($index == 0) {
				$this->addClass($itemProps['liAttrs'], 'first');
			}
			if (($totalLength - 1) == $index) {
				$this->addClass($itemProps['liAttrs'], 'last');
			}
			
			/*
			 * Default values
			 */
			$label = '';
			$url = '';
			$sub = '';
			$liAttrsLine = '';
			$aAttrsLine = '';

			/*
			 * Loop through item properties
			 */
			foreach($itemProps as $propKey => $propVal) {
				/*
				 * If submenu - run method again recursively
				 */
				switch ($propKey) {
					case 'label':
						$label = $propVal;
						break;
					case 'url':
						$url = $propVal;
						break;
					case 'aAttrs':
						$aAttrsLine = $this->generateAttributesLine($propVal);
						break;
					case 'liAttrs':
						$liAttrsLine = $this->generateAttributesLine($propVal);
						break;
					/*
					 * Submenu found - so we simply build submenu in recursive fashion
					 */
					case 'sub':
						if (is_array($propVal) && (count( $propVal) !== 0)) {
							$sub = $this->recursiveMenu($cnt + 1, $propVal, array_key_exists('ulAttrs', $itemProps) ? $itemProps['ulAttrs'] : array());
						}
						break;
				}
			}
			
			/*
			 * Befores and afters
			 */
			$aBefore = array_key_exists('aBefore', $itemProps) && is_string($itemProps['aBefore']) ? $itemProps['aBefore'] : '';
			$aAfter = array_key_exists('aAfter', $itemProps) && is_string($itemProps['aAfter']) ? $itemProps['aAfter'] : '';
			$liBefore = array_key_exists('liBefore', $itemProps) && is_string($itemProps['liBefore']) ? $itemProps['liBefore'] : '';
			$liAfter = array_key_exists('liAfter', $itemProps) && is_string($itemProps['liAfter']) ? $itemProps['liAfter'] : '';
			$labelBefore = array_key_exists('labelBefore', $itemProps) && is_string($itemProps['labelBefore']) ? $itemProps['labelBefore'] : '';
			$labelAfter = array_key_exists('labelAfter', $itemProps) && is_string($itemProps['labelAfter']) ? $itemProps['labelAfter'] : '';
			
			/*
			 * Use the created variables to output HTML
			 */
			$label = !empty($this->translator) ? $this->translator->translate($label) : $label;
			$label =  $labelBefore . $label . $labelAfter;
			$urlPlace = ($url === '') ? '<span>' . $label . '</span>' : '<a' . $aAttrsLine . ' href="' . $url . '">' . $label . '</a>';
			$menu .= $liBefore . '<li' . $liAttrsLine . '>' . $aBefore. $urlPlace . $aAfter  . $sub . '</li>' . $liAfter;
			
			/*
			 * Unset between iterations
			 */
			unset($label, $url, $sub, $urlPlace, $liAttrsLine, $aAttrsLine);
		}
		
		/*
		 * Close the menu container and return the markup for output
		 */
		return ($menu . '</ul>');
	}
	
	private function generateAttributesLine(array $attributes)
	{
		$attributesLine = '';
		foreach ($attributes as $attrKey => $attrVal) {
			$attributesLine .= ' ' . $attrKey . ' = "' . $attrVal . '"';
		}
		return $attributesLine;
	}
}